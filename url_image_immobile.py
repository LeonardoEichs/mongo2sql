from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from base import Base
class Url_Image_Immobile(Base):

    """Url_Image_Immobile.

    Atributos do banco de dados de imagens
    """

    __tablename__ = 'url_image_immobile'
    id = Column(Integer, primary_key=True)
    cd_imobiliaria = Column(Integer)
    cd_imovel = Column(String(100))
    url = Column(String)
    #id_opr = Column(String(1))

    def __repr__(self):

        u"""Método para retorno."""

        return "<User(id='%i', cd_imobiliaria='%i', cd_imovel='%s', url='%s')>" % (
        self.id, self.cd_imobiliaria, self.cd_imovel, self.url)

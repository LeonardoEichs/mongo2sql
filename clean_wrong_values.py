# Mongo2SQL
from session import session
from engine import engine
from url_image_immobile import Url_Image_Immobile
from immobile_crawled import Immobile_Crawled
from sqlalchemy.exc import IntegrityError
from mongo import collection
from utils import *
import datetime
import atexit
import time
import sys

#################################

PRECO_ALUGUEL = 30000.0
PRECO_TEMPORADA = 3000.0

#################################

mark_for_deletion = []
mark_for_deletion_temp = []

aluguel = session.query(Immobile_Crawled).filter_by(
    id_opr="A"
    )

temporada = session.query(Immobile_Crawled).filter_by(
    id_opr="T"
    )

for immobile in aluguel:
    try:
        if float(immobile.vl_alu_com_desc) >= PRECO_ALUGUEL:
            print("--------------------")
            print(immobile.url)
            print(immobile.vl_alu_com_desc)
            mark_for_deletion.append(immobile)
    except:
        print("--------------------")
        print("Aluguel sem valor")
        print(immobile.url)

for immobile in temporada:
    try:
        if float(immobile.vl_alu_com_desc) >= PRECO_TEMPORADA:
            print("--------------------")
            print(immobile.url)
            print(immobile.vl_alu_com_desc)
            mark_for_deletion_temp.append(immobile)
    except:
        print("--------------------")
        print("Temporada sem valor")
        print(immobile.url)

counter_aluguel = aluguel.count()
counter_temporada = temporada.count()

print("-------------------------------")
print("Qtd p/ Aluguel: \t" + str(counter_aluguel))
print("Qtd p/ Temporada:\t" + str(counter_temporada))
print("-------------------------------")
try:
    mark_for_deletion.sort(key=Immobile_Crawled.return_vl_alu)
    print("Marked for Deletion Aluguel:\t" + str(len(mark_for_deletion)))
    print("Lower value immobile:")
    print("\t" + mark_for_deletion[0].url)
    print("\t" +str(mark_for_deletion[0].vl_alu_com_desc))
    print("-------------------------------")
except:
    print("None")
    pass
try:
    mark_for_deletion.sort(key=Immobile_Crawled.return_vl_alu)
    print("Marked for Deletion Temporada:\t" + str(len(mark_for_deletion_temp)))
    print("Lower value immobile:")
    print("\t" + mark_for_deletion_temp[0].url)
    print("\t" + str(mark_for_deletion_temp[0].vl_alu_com_desc))
    print("-------------------------------")
except:
    print("None")
    pass

print("Press 1 to delete only Aluguel")
print("Press 2 to delete only Temporada")
print("Press 3 to delete only Both")
print("Press 0 to Quit")
answ = input(">> ")
count = 0
if answ == "1":
    print("Delete only Aluguel")
    for i in mark_for_deletion:
        session.delete(i)
        session.commit()
        count += 1
elif answ == "2":
    print("Delete only Temporada")
    for i in mark_for_deletion_temp:
        session.delete(i)
        session.commit()
        count += 1
elif answ == "3":
    print("Delete Both")
    for i in mark_for_deletion:
        session.delete(i)
        session.commit()
        count += 1
    for i in mark_for_deletion_temp:
        session.delete(i)
        session.commit()
        count += 1
else:
    print("Quit")
    sys.exit(0)

print("Deleted " + str(count))

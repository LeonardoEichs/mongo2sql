# Mongo2SQL
from session import session
from engine import engine
from url_image_immobile import Url_Image_Immobile
from immobile_crawled import Immobile_Crawled
from sqlalchemy.exc import IntegrityError
from mongo import collection
from utils import *
import datetime
import atexit
import time


def exit_handler():
    engine.execute(Immobile_Crawled.__table__.insert(), newImmobiles_list)
    engine.execute(Url_Image_Immobile.__table__.insert(), newImg_list)
    session.commit()

atexit.register(exit_handler)

counter = 0

newImmobiles_list = []
newImg_list = []

for post in collection.find():
    print("#####=======#####")
    counter += 1

    user = session.query(Immobile_Crawled).filter_by(
        cd_imovel=str(post["cd_estate"]),
        cd_imobiliaria=str(post["cd_real_estate"]),
        id_opr=str(post["id_opr"])
     )
    print(counter)
    if post["id_opr"] == "V":
        b_locacao = "F"
        b_venda = "T"
        aluguel = 0.0
        venda = str_to_float(post["values"]["price"][0][1])
    else:
        b_locacao = "T"
        b_venda = "F"
        venda = 0.0
        aluguel = str_to_float(post["values"]["price"][0][1])
    if post["type"]["code"] == 6:
        areap = 0.0
        areat = str_to_float(post["area"])
    else:
        areap = str_to_float(post["area"])
        areat = 0.0
    new = {"cd_imobiliaria": post["cd_real_estate"],
           "cd_imovel": post["cd_estate"],
           "ib_ativo": "T" if post["active"] else "F",
           "ib_locacao": b_locacao,
           "ib_venda": b_venda,
           "id_opr": post["id_opr"],
           "ds_endereco": post["localization"]["address"],
           "id_finalidade": post["id_finality"],
           "vl_alu_com_desc": aluguel,
           "vl_venda": venda,
           "vl_imovel": str_to_float(post["values"]["indexing_price"]),
           "vl_iptu": post["values"]["IPTU"],
           "vl_condominio": post["values"]["condominium"],
           "nr_quartos": empty_to_zero(post["numbers"]["bedrooms"]),
           "nr_suites": empty_to_zero(post["numbers"]["suites"]),
           "nr_salas": empty_to_zero(post["numbers"]["rooms"]),
           "nr_banheiros": empty_to_zero(post["numbers"]["bathrooms"]),
           "ib_sacada": "T" if post["booleans"]["balcony"] else "F",
           "ib_area_servico": "T" if post["booleans"]["service_area"] else "F",
           "ib_dep_empregada": "T" if post["booleans"]["maids_room"] else "F",
           "ib_gas_central": "T" if post["booleans"]["central_gas"] else "F",
           "ib_mobiliado": "T" if post["booleans"]["furniture"] else "F",
           "nr_garagem": empty_to_zero(post["numbers"]["garage"]),
           "ib_piscina": "T" if post["booleans"]["pool"] else "F",
           "ib_salao_festas": "T" if post["booleans"]["party_hall"] else "F",
           "ib_interfone": "T" if post["booleans"]["intercom"] else "F",
           "ib_portao_elet": "T" if post["booleans"]["electric_gate"] else "F",
           "nr_elevador": 1 if post["booleans"]["elevator"] else 0,
           # "nr_apto_andar": none,
           "vl_area_privativa": areap,
           "vl_area_terreno": areat,
           # "ds_idade": none,
           # "ds_titulo_site": none,
           "ds_site": post["description"],
           "dataultatualizacao": datetime.datetime.fromtimestamp(
           int(post["dates"]["date_last_update"])).strftime("%Y-%m-%d %H:%M:%S")
           if post["dates"]["date_last_update"] is not None else None,
           # "latitudelongitude": none,
           "lat": slice(post["localization"]["coordinate"]["lat"], 0, 12),
           "lng": slice(post["localization"]["coordinate"]["lng"], 0, 12),
           "dataentrada": datetime.datetime.fromtimestamp(
           int(post["dates"]["entry_date"])).strftime("%Y-%m-%d %H:%M:%S")
           if post["dates"]["entry_date"] is not None else None,
           "datasaida": datetime.datetime.fromtimestamp(
           int(post["dates"]["exit_date"])).strftime("%Y-%m-%d %H:%M:%S")
           if post["dates"]["exit_date"] is not None else None,
           "cidade": post["localization"]["city"]["name"],
           "bairro": post["localization"]["neighborhood"]["name"],
           "estado": post["localization"]["state"],
           "tipo": post["type"]["name"],
           "url": post["url"],
           "nr_views": post["nr_views"],
           "bairro_fk": post["localization"]["neighborhood"]["code"],
           "tipo_fk": post["type"]["code"]
           }

    print("Imovel: " + str(new["cd_imovel"]) + "\tImobiliaria: " + str(new["cd_imobiliaria"]))

    if (user.count() == 0):
        print("NOVO IMOVEL")
        newImmobiles_list.append(new)
    else:
        del new["cd_imobiliaria"]
        del new["cd_imovel"]
        del new["id_opr"]
        if (user.count() > 1):
            user = user.filter_by(id_opr=str(post["id_opr"]))
        user.update(new)
        session.commit()

    imagessql = session.query(Url_Image_Immobile).filter_by(
        cd_imovel=str(post["cd_estate"]),
        cd_imobiliaria=str(post["cd_real_estate"])
    )

    for image in imagessql:
        session.delete(image)
    session.commit()
    for each_img in post["images"]:
        newImg_list.append({
                "cd_imobiliaria": post["cd_real_estate"],
                "cd_imovel": post["cd_estate"],
                "url": each_img,
                "id_opr": post["id_opr"]
                })
    try:
        engine.execute(Immobile_Crawled.__table__.insert(), newImmobiles_list)
        engine.execute(Url_Image_Immobile.__table__.insert(), newImg_list)
        session.commit()
        print("INSERTION")
    except IntegrityError as e:
        if not e[0] == 1062:
            raise
        else:
            print("[1062]")
            pass
    newImmobiles_list = []
    newImg_list = []
    time.sleep(0.01)

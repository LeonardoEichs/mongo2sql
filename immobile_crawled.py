from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from base import Base
class Immobile_Crawled(Base):

    __tablename__ = 'immobile_crawled'
    id = Column(Integer, primary_key=True)
    cd_imobiliaria = Column(Integer)
    cd_imovel = Column(String(100))
    ib_ativo = Column(String)
    ib_locacao = Column(String)
    ib_venda = Column(String)
    id_opr = Column(String(1))
    ds_endereco = Column(String)
    id_finalidade = Column(String)
    vl_alu_com_desc = Column(String)
    vl_imovel = Column(Float)
    vl_venda = Column(Float)
    vl_iptu = Column(Float)
    vl_condominio = Column(Float)
    nr_quartos = Column(Integer)
    nr_suites = Column(Integer)
    nr_salas = Column(Integer)
    nr_banheiros = Column(Integer)
    ib_sacada = Column(String)
    ib_area_servico = Column(String)
    ib_dep_empregada = Column(String)
    ib_gas_central = Column(String)
    ib_mobiliado = Column(String)
    nr_garagem = Column(Integer)
    ib_piscina = Column(String)
    ib_salao_festas = Column(String)
    ib_interfone = Column(String)
    ib_portao_elet = Column(String)
    nr_elevador = Column(Integer)
    nr_apto_andar = Column(Integer)
    vl_area_privativa = Column(Float)
    vl_area_terreno = Column(Float)
    ds_idade = Column(String)
    ds_titulo_site = Column(String)
    ds_site = Column(String)
    dataultatualizacao = Column(String)
    latitudelongitude = Column(String)
    lat = Column(String)
    lng = Column(String)
    dataentrada = Column(String)
    datasaida = Column(String)
    cidade = Column(String)
    bairro = Column(String)
    estado = Column(String)
    tipo = Column(String)
    url = Column(String)
    nr_views = Column(Integer)
    bairro_fk = Column(Integer)
    tipo_fk = Column(Integer)

    def __repr__(self):

        return "<User(id='%i', cd_imobiliaria='%i', cd_imovel='%s', ib_ativo='%s', ib_locacao='%s', ib_venda='%s', id_opr='%s',\
        ds_endereco='%s', id_finalidade='%s', vl_alu_com_desc='%s', vl_imovel='%f', vl_venda='%f', vl_iptu='%f', vl_condominio='%f',\
        nr_quartos='%i', nr_suites='%i', nr_salas='%i', nr_banheiros='%i', ib_sacada='%s', ib_area_servico='%s', ib_dep_empregada='%s',\
        ib_gas_central='%s', ib_mobiliado='%s', nr_garagem='%i', ib_piscina='%s', ib_salao_festas='%s', ib_interfone='%s',\
        ib_portao_elet='%s', nr_elevador='%i', nr_apto_andar='%i', vl_area_privativa='%f', vl_area_terreno='%f', ds_idade='%s',\
        ds_titulo_site='%s', ds_site='%s', dataultatualizacao='%s', latitudelongitude='%s', lat='%s', lng='%s', dataentrada='%s',\
        datasaida='%s', cidade='%s', bairro='%s', estado='%s', tipo='%s', url='%s', nr_views='%i', bairro_fk='%i', tipo_fk='%i')>" % (
        self.id, self.cd_imobiliaria, self.cd_imovel, self.ib_ativo, self.ib_locacao, self.ib_venda, self.id_opr, self.ds_endereco,
        self.id_finalidade, self.vl_alu_com_desc, self.vl_imovel, self.vl_venda, self.vl_iptu, self.vl_condominio, self.nr_quartos,
        self.nr_suites, self.nr_salas, self.nr_banheiros, self.ib_sacada, self.ib_area_servico, self.ib_dep_empregada, self.ib_gas_central,
        self.ib_mobiliado, self.nr_garagem, self.ib_piscina, self.ib_salao_festas, self.ib_interfone, self.ib_portao_elet, self.nr_elevador,
        self.nr_apto_andar, self.vl_area_privativa, self.vl_area_terreno, self.ds_idade, self.ds_titulo_site, self.ds_site, self.dataultatualizacao,
        self.latitudelongitude, self.lat, self.lng, self.dataentrada, self.datasaida, self.cidade, self.bairro, self.estado, self.tipo, self.url,
        self.nr_views, self.bairro_fk, self.tipo_fk)

    def return_vl_alu(self):
        return self.vl_alu_com_desc

# Mongo2SQL
from session import session
from engine import engine
from url_image_immobile import Url_Image_Immobile
from immobile_crawled import Immobile_Crawled
from sqlalchemy.exc import IntegrityError
from mongo import collection
from utils import *
import datetime
import atexit
import time

counter_ok = 0
counter_del = 0

for post in session.query(Immobile_Crawled):
    print("#####=======#####")

    user = collection.find({"cd_real_estate":post.cd_imobiliaria, "cd_estate":post.cd_imovel, "id_opr":post.id_opr})

    if user.count() > 0:
        counter_ok += 1
        print("OK, immobile on Mongo")
    else:
        print(post.dataultatualizacao)
        try:
            date_object = datetime.datetime.strptime(post.dataultatualizacao, '%Y-%m-%d %H:%M:%S')
        except:
            print("Wrong date format")
            print("\tWrong Data:\t" + str(post.dataultatualizacao))
            print("\tCd Imobiliaria:\t" + str(post.cd_imobiliaria))
            print("\tCd Imovel:\t" + str(post.cd_imovel))
            #break
        if date_object > datetime.datetime.strptime('2016-08-01 00:00:00', '%Y-%m-%d %H:%M:%S'):
            print(date_object)
            counter_del += 1
            session.delete(post)
            #session.commit()
            print("Remover os novos")
            print("Delete")

session.commit()
print("-------------")
print("Ok:\t" + str(counter_ok))
print("-------------")
print("Delete:\t" + str(counter_del))
print("-------------")
print("Total:\t" + str(counter_ok + counter_del))
print("Total on SQL:\t" + str(session.query(Immobile_Crawled).count()))
print("-------------")

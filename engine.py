from sqlalchemy import create_engine

try:
    import pymysql
    pymysql.install_as_MySQLdb()
except ImportError:
    pass
engine = create_engine('mysql+pymysql://root:root@localhost/zilha?charset=utf8', echo=False)

def str_to_float(text):
    if (type(text).__name__ == "str"):
        text =text.replace(".", "")
        text =text.replace(",", ".")
        return text
    elif(text is not None):
        return text
    return None

def slice(text, start, end):
    if (type(text).__name__ == "str"):
        return text[start:end]
    elif (text is not None):
        return text
    return None

def empty_to_zero(text):
    if (text == ""):
        return 0
    elif (text is not None):
        return text
    return None
